BEGIN{
	molecs= ()
	molecsind=0
	cadena = awk 'NR == 1 {print $5}' $name.txt
	res = `awk 'NR == 1 {print $4}' $name.txt`
	nres= `awk 'NR == 1 {print $6}' $name.txt`
	mol = `awk 'NR == 1 {print $3}' $name.txt`
	nmol = `awk 'NR == 1 {print $2}' $name.txt`
	echo "digraph G {" >> $name.dot
	echo "rankdir=LR;" >> $name.dot
	echo 'size="8,5"' >> $name.dot
	echo "node [shape=box]" >> $name.dot
	segundores=`awk '$6 = $nres +1 {print $4}' $name.txt | awk 'NR == 1 {print $0}'`
	echo '$res -> $segundores [label = "Cadena $cadena"];' >> $name.dot
	echo "node [shape=circle]" >> $name.dot
	}
{	
	if ( "$5" == "$cadena" ) {
		
		if ( $6 == $nres ) {
			for (i=0; i=$molecsind || molecs[i]=""; i++){
				if ( "${molecs[$i]}" = "$3" ){
					
					enlista=1
				}
			}	
			if (! $enlista == 1){
				molecs=("$molecs[@]" "$3")
				molecsind++
			}
			echo "$3$5 -> $3" >> $name.dot
			enlista=0
		}					
		else{	
			nres++
			echo "node [shape=box]" >> $name.dot
			
			res= `awk '$6 = $nres {print $4}' $name.txt | awk 'NR == 1 {print $0}'`
			segundores=`awk '$6 = $nres +1 && $5 = $cadena {print $4}' $name.txt | awk 'NR == 1 {print $0}'`
			segundonres=`awk '$6 = $nres +1 && $5 = $cadena {print $6}' $name.txt | awk 'NR == 1 {print $0}'`
			if ( $segundonres != $nres){
				echo '$res -> $segundores [label = "Cadena $cadena"];' >> $name.dot
			}
			for (i=0; i=$molecsind || molecs[i]=""; i++){
				if ( "${molecs[$i]}" = "$3" ){
					
					enlista=1
				}
			}	
			if (! $enlista == 1){
				molecs=("$molecs[@]" "$3")
				molecsind++
			}
			echo "$3$5 -> $3" >> $name.dot
			enlista=0
		}	
	}
	else {
		cadena= $5
		if ( $6 == $nres ) {
			for (i=0; i=$molecsind || molecs[i]=""; i++){
				if ( "${molecs[$i]}" = "$3" ){
					
					enlista=1
				}
			}	
			if (! $enlista == 1){
				molecs=("$molecs[@]" "$3")
				molecsind++
			}
			echo "$3$5 -> $3" >> $name.dot
			enlista=0
		}					
		else{
			nres++
			echo "node [shape=box]" >> $name.dot
			
			res= `awk '$6 = $nres {print $4}' $name.txt | awk 'NR == 1 {print $0}'`
			segundores=`awk '$6 = $nres +1 && $5 = $cadena {print $4}' $name.txt | awk 'NR == 1 {print $0}'`
			segundonres=`awk '$6 = $nres +1 && $5 = $cadena {print $6}' $name.txt | awk 'NR == 1 {print $0}'`
			if ( $segundonres != $nres){
				echo '$res -> $segundores [label = "Cadena $cadena"];' >> $name.dot
			}
			for (i=0; i=$molecsind || molecs[i]=""; i++){
				if ( "${molecs[$i]}" = "$3" ){
					
					enlista=1
				}
			}	
			if (! $enlista == 1){
				molecs=("$molecs[@]" "$3")
				molecsind++
			}
			echo "$3$5 -> $3" >> $name.dot
			enlista=0
		}	
	}
}
END{
		echo "}" >> $name.dot
}
	
				
			
			
			
	
	
	
	
	
	

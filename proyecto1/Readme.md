**Visualizador de proteinas**
El programa busca la visualizacion de a partir de un archivo .pdb, para esto se manipula dicho archivo de tal manera que la data tenga un uso mas práctica.
Para que el programa se ejecute correctamente se debe ingresar el codigo alfanumerico de una proteína que se encuentre dentro de las proteínas permitidas (base de datos), de no ser así se le pedirá al usuario 
que ingrese una proteína nuevamente.
Se necesita conexión a internet, para descargar los archivos de pdb y la base de datos.
El programa se compone de un script Bash llamado "comprobador.sh" y de un archivo de tipo .awk, el cual funcionaría como filtro.
Se hace uso del lenguaje de programacion Bash para la creación del programa, así mismo también se utiliza Awk. 

#!/bin/bash
#Se le pide al usuario el codigo alfanumerico de la proteína a graficar, este dato ingresado puede ser tanto en minusculas como mayusculas
#ya que se le modifica para que sea mayusculas y así ser comparado en la base de datos, la cual tambien fue modificada para considerar solo 
#los caracteres alfanumericos de las filas que hayan sido "Protein"
existe=0
wget https://icb.utalca.cl/~fduran/bd-pdb.txt
while :; do
echo "------------Bienvenido al Visualizador Proteico-------------"
echo "Ingrese el nombre del archivo a graficar, esta debe ser reconocida por la base de datos"
read name 
cat bd-pdb.txt |grep '"Protein"$'| awk -F"," '{print substr($1,2,4)} ' > alfanum.txt
database=`grep -io "$name" ./alfanum.txt`
name=`echo $name |awk '{print toupper($0)}'`
#aqui comparamos lo ingresado con la database
if [ "${name}" = "${database}" ]; then
	existe=1
	echo "Su proteína ha sido aceptada segun la base de datos."
	wget https://files.rcsb.org/download/$name.pdb
	echo "Descargada"
	cat $name.pdb |grep '^ATOM' | awk '{print $1 " " $2 " " $3 " " $4 " " $5 " " $6}' > $name.txt
	echo "Ahora su proteína procederá a ser graficada"
	#En este punto existen complicaciones, mi pc no me permitió descargar graphviz, por lo tanto no estoy seguro si funciona correctamente desde aquí.
	#Si todo va de acuerdo a como lo pensé, debería graficarse correctamente.
	awk -v CONVFMT='%.0f ' -f graficador.awk $name.txt 
	dot - Tpng -o $name.png $name.dot
	echo "Muchas gracias por usar el software, si nuestra ayuda le ha servido no dude en transferir la cantidad que usted estime conveniente a la cuenta rut 20007855 banco estado."
	break
else
	#al no encontrar coincidencia, se le pide al usuario que lo vuelva a intantar
	echo "No se ha encontrado coincidencias con la proteína ingresada en la base de datos, intente nuevamente con una proteína que se encuentre en la base de datos."	
	existe=0
fi
done
